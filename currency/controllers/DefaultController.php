<?php

namespace app\modules\currency\controllers;

use yii\web\Controller;

/**
 * Default controller for the `currency` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCurrency()
    {
        return $this->render('currency');
    }

       public function actionCurrency()
    {
        if (Yii::$app->request->isAjax) {
    $data = Yii::$app->request->post();
    //print_R($data);die();
 
$amount    = urlencode($data['as']);

$from    = urlencode($data['from']);
//print_R($from);die();
$to        = urlencode($data['to']);
$url = "https://www.google.com/search?q=".$from."+to+".$to;
//$url    = "http://www.google.com/ig/calculator?hl=en&q=$amount$from=?$to";
$ch     = @curl_init();
$timeout= 0;
 
curl_setopt ($ch, CURLOPT_URL, $url);
curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt ($ch,  CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
 
$rawdata = curl_exec($ch);
curl_close($ch);
$data = preg_split('/\D\s(.*?)\s=\s/',$rawdata);
$exhangeRate = (float) substr($data[1],0,7);
$convertedAmount = $amount*$exhangeRate;

 return  $convertedAmount;
      
    }
}
}
