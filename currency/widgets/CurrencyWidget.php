<?php
namespace app\modules\currency\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;

class CurrencyWidget extends Widget{
    public $message;

    public function init(){
        parent::init();
    }

    public function run(){
        return $this->render('currency');
    }
}
?>